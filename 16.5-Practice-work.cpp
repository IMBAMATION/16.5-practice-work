﻿#include <iostream>
#include <time.h>
using namespace std;

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int day = buf.tm_mday;
    int const N = 3;
    int sum;
    int sumString = 0;

    int array[N][N];

    for (int i = 0; i < N; ++i)
    {
        cout << endl;
        for (int j = 0; j < N; ++j)
        {
            array[i][j] = i + j;
            cout << array[i][j] << " ";

        }
    }
    for (int j = 0; j < N; ++j)
    {
        sumString = sumString + array[day % N][j];
    }

    cout << endl;
    cout << "today " << day << endl;
    cout << "String sum = " << sumString;

}